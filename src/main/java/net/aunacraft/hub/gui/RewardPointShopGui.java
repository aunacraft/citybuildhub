package net.aunacraft.hub.gui;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.api.util.SkullBuilder;
import net.aunacraft.hub.reward.player.RewardPlayer;
import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.model.user.User;
import net.luckperms.api.node.Node;
import net.luckperms.api.node.types.InheritanceNode;
import net.luckperms.api.node.types.PermissionNode;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.concurrent.TimeUnit;

public class RewardPointShopGui extends RewardGui {

    private static final ItemStack ARROW_LEFT_ITEM_STACK = SkullBuilder.getSkull("http://textures.minecraft.net/texture/37aee9a75bf0df7897183015cca0b2a7d755c63388ff01752d5f4419fc645");

    public RewardPointShopGui(RewardPlayer rewardPlayer) {
        super(rewardPlayer, rewardPlayer.toAunaPlayer().getMessage("hub.gui.reward.title", rewardPlayer.getRewardPoints()), 9 * 5);
        this.rewardPlayer = rewardPlayer;
    }

    @Override
    public void initInventory(AunaPlayer player, GuiInventory inventory) {

        inventory.setItem(9 * 4, new ItemBuilder(ARROW_LEFT_ITEM_STACK).setDisplayName(player, "api.gui.back").build(), event -> {
            new RewardGuiIndex(this.rewardPlayer).open(player);
        });

        setShopItem(player, 11, 1, new ItemBuilder(Material.GOLD_NUGGET).setDisplayName("§e500 Coins").build(), () -> {
            AunaAPI.getApi().getCoinService().addCoins(player.getUuid(), 500, () -> player.sendMessage("hub.gui.shop.coins_added", 500));
        });

        setShopItem(player, 20, 10, new ItemBuilder(Material.GOLD_INGOT).setDisplayName("§e6.000 Coins").build(), () -> {
            AunaAPI.getApi().getCoinService().addCoins(player.getUuid(), 6000, () -> player.sendMessage("hub.gui.shop.coins_added", 6000));
        });

        setShopItem(player, 29, 25, new ItemBuilder(Material.RAW_GOLD).setDisplayName("§e23.000 Coins").build(), () -> {
            AunaAPI.getApi().getCoinService().addCoins(player.getUuid(), 23000, () -> player.sendMessage("hub.gui.shop.coins_added", 23000));
        });

        setRankShopItem(
                player,
                15,
                400,
                new ItemBuilder(Material.NETHERITE_CHESTPLATE)
                        .setDisplayName(player, "hub.gui.shop.rank", 30, "§cKing")
                        .build(),
                "king",
                30);

        setRankShopItem(
                player,
                24,
                200,
                new ItemBuilder(Material.DIAMOND_CHESTPLATE)
                        .setDisplayName(player, "hub.gui.shop.rank", 30, "§3Elite")
                        .build(),
                "elite",
                30);

        setRankShopItem(player,
                33,
                50,
                new ItemBuilder(Material.GOLDEN_CHESTPLATE)
                        .setDisplayName(player, "hub.gui.shop.rank", 10, "§ePrime")
                        .build(),
                "prime",
                10);

        inventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName("§c ").build());

    }

    private void setShopItem(AunaPlayer player, int slot, int coasts, ItemStack stack, Runnable buyCallback) {
        stack = new ItemBuilder(stack).setLore(player.getMessage("hub.gui.shop.item.lore.coasts", coasts)).build();
        this.inventory.setItem(slot, stack, event -> {
            if(this.rewardPlayer.getRewardPoints() >= coasts) {
                rewardPlayer.removeRewardPoints(coasts);
                new RewardPointShopGui(this.rewardPlayer).open(player);
                buyCallback.run();
                this.savePlayer();
            }else {
                player.toBukkitPlayer().playSound(player.toBukkitPlayer().getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 2, 1);
                player.sendMessage("hub.gui.shop.error.points");
            }
        });
    }

    private void setRankShopItem(AunaPlayer player, int slot, int coasts, ItemStack stack, String rankName, int time) {
        if (hasRank(player, rankName)) {
            stack = new ItemBuilder(stack).setLore(player.getMessage("hub.gui.shop.item.lore.rank.own", coasts)).build();
            inventory.setItem(slot, stack);
            return;
        }
        setShopItem(player, slot, coasts, stack, () -> {
            player.toBukkitPlayer().closeInventory();
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "lp user "
                    + player.getName() +
                    " parent addtemp " + rankName +
                    " " + time + "d");
        });
    }

    private boolean hasRank(AunaPlayer player, String rankName) {
        return player.hasPermission("group." + rankName);
    }
}
