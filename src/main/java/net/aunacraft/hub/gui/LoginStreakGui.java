package net.aunacraft.hub.gui;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.gui.page.PageGui;
import net.aunacraft.api.gui.page.Pageable;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.AunaCallable;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.api.util.SkullBuilder;
import net.aunacraft.hub.HubPlugin;
import net.aunacraft.hub.reward.loginstreak.LoginStreak;
import net.aunacraft.hub.reward.loginstreak.LoginStreakReward;
import net.aunacraft.hub.reward.player.RewardPlayer;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class LoginStreakGui extends PageGui {

    private static final ItemStack ARROW_LEFT_ITEM_STACK = SkullBuilder.getSkull("http://textures.minecraft.net/texture/37aee9a75bf0df7897183015cca0b2a7d755c63388ff01752d5f4419fc645");

    private final RewardPlayer rewardPlayer;

    public LoginStreakGui(RewardPlayer player, List<LoginStreakReward> rewards) {
        super(player.toAunaPlayer().getMessage("hub.gui.loginstreak.name", player.getLoginStreak().getStreak()), 9 * 2, Lists.newArrayList(convertRewardList(player, rewards)));
        this.rewardPlayer = player;
    }

    @Override
    public void preInit(AunaPlayer aunaPlayer, GuiInventory inventory, int invSize) {
        inventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName("§c ").build());
        for(int i = 9; i < 18; i++) {
            inventory.setItem(i, null);
        }
    }

    @Override
    public void postInit(AunaPlayer aunaPlayer, GuiInventory inventory, int invSize) {
        inventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName("§c ").build());
        inventory.setItem(18, new ItemBuilder(ARROW_LEFT_ITEM_STACK).setDisplayName(aunaPlayer, "api.gui.back").build(), event -> {
            new RewardGuiIndex(this.rewardPlayer).open(aunaPlayer);
        });
    }

    @Override
    protected int getAmountPerPage() {
        return 9;
    }

    private static List<Pageable> convertRewardList(RewardPlayer player, List<LoginStreakReward> rewards) {
        List<Pageable> rewardPageables = Lists.newArrayList();
        rewards.forEach(reward -> rewardPageables.add(new LoginStreakRewardPageable(reward, player)));
        return rewardPageables;
    }

    @Getter
    @AllArgsConstructor
    private static class LoginStreakRewardPageable implements Pageable {

        private final LoginStreakReward reward;
        private final RewardPlayer player;

        @Override
        public void onClick(PageGui pageGui, AunaPlayer aunaPlayer) {
            if (player.getLoginStreak().canCollectReward(reward)) {
                player.getLoginStreak().setRewardCollected(reward);
                reward.givePlayer(player);
                HubPlugin.getInstance().getRewardPlayerRepository().save(player);
                aunaPlayer.toBukkitPlayer().playSound(aunaPlayer.toBukkitPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 2, 1);
                pageGui.inventory.clear();
                pageGui.initInventory(aunaPlayer, pageGui.inventory);
            }else {
                aunaPlayer.toBukkitPlayer().playSound(aunaPlayer.toBukkitPlayer().getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 2, 1);
            }
        }

        @Override
        public ItemStack getIconStack() {
            LoginStreak loginStreak = player.getLoginStreak();
            Material material = loginStreak.hasCollectedReward(reward) ? Material.MINECART : loginStreak.canCollectReward(reward) ? Material.CHEST_MINECART : Material.TNT_MINECART;

            ItemBuilder builder = new ItemBuilder(material).setDisplayName(player.toAunaPlayer(),"hub.gui.loginstreak.item.reward.name", this.reward.getStreak());
            if(loginStreak.canCollectReward(reward)) {
                builder.setLore(player.toAunaPlayer().getMessage("hub.gui.loginstreak.item.reward.lore.collectable"));
            }else if(loginStreak.hasCollectedReward(reward)) {
                builder.setLore(player.toAunaPlayer().getMessage("hub.gui.loginstreak.item.reward.lore.collected"));
            }else {
                builder.setLore(
                        player.toAunaPlayer().getMessage("hub.gui.loginstreak.item.reward.lore.not_available.1"),
                        player.toAunaPlayer().getMessage("hub.gui.loginstreak.item.reward.lore.not_available.2", reward.getStreak())
                        );
            }

            return builder.build();
        }
    }
}
