package net.aunacraft.hub.gui;

import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.hub.HubPlugin;
import net.aunacraft.hub.reward.player.RewardPlayer;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;

public class RewardGuiIndex extends RewardGui {

    public RewardGuiIndex(RewardPlayer rewardPlayer) {
        super(rewardPlayer, rewardPlayer.toAunaPlayer().getMessage("hub.gui.reward.title", rewardPlayer.getRewardPoints()), 9 * 5);
    }

    @Override
    public void initInventory(AunaPlayer player, GuiInventory inventory) {

        inventory.setItem(13, new ItemBuilder(Material.RAW_GOLD)
                        .setDisplayName(player, "hub.gui.reward.shop.name")
                        .setLore(player.getMessage("hub.gui.reward.shop.lore.1"), player.getMessage("hub.gui.reward.shop.lore.2"))
                        .addItemFlags(ItemFlag.HIDE_ENCHANTS)
                        .addEnchant(Enchantment.WATER_WORKER, 1)
                        .build()
        , event -> new RewardPointShopGui(this.rewardPlayer).open(player));

        setDailyRewardItem(player, inventory);

        inventory.setItem(30,
                new ItemBuilder(Material.CLOCK)
                        .setDisplayName(player, "hub.gui.reward.loginstreak.name")
                        .setLore(
                                player.getMessage("hub.gui.reward.loginstreak.lore.1"),
                                player.getMessage("hub.gui.reward.loginstreak.lore.2"),
                                player.getMessage("hub.gui.reward.loginstreak.lore.3"),
                                player.getMessage("hub.gui.reward.loginstreak.lore.4"),
                                player.getMessage("hub.gui.reward.loginstreak.lore.5")
                        )
                        .build()
        , event -> new LoginStreakGui(this.rewardPlayer, HubPlugin.getInstance().getLoginStreakHandler().getRewards()).open(player));

        inventory.setItem(32,
                new ItemBuilder(Material.SPYGLASS)
                        .setDisplayName(player, "hub.gui.reward.onlinetime.name")
                        .setLore(
                                player.getMessage("hub.gui.reward.onlinetime.lore.1"),
                                player.getMessage("hub.gui.reward.onlinetime.lore.2")
                        )
                        .build()

        , event -> new OnlineTimeRewardGui(this.rewardPlayer, HubPlugin.getInstance().getOnlineTimeRewardHandler().getRewards()).open(player));

        inventory.setItem(34,
                new ItemBuilder(Material.PAPER)
                        .setDisplayName(player, "hub.gui.reward.vote.name")
                        .setLore(
                              player.getMessage("hub.gui.reward.vote.lore.1"),
                              player.getMessage("hub.gui.reward.vote.lore.2"),
                              player.getMessage("hub.gui.reward.vote.lore.3")
                        )
                        .build()
        );

        inventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName("§c ").build());

    }

    private void setDailyRewardItem(AunaPlayer player, GuiInventory inventory) {
        boolean collectable = rewardPlayer.getDailyReward().isCollectable();
        Material itemStackType = collectable ? Material.CHEST_MINECART : Material.MINECART;
        String loreKey = collectable ? "hub.gui.reward.daily.lore.status.collectable" : "hub.gui.reward.daily.lore.status.already_collected";

        inventory.setItem(28, new ItemBuilder(itemStackType)
                .setDisplayName(player, "hub.gui.reward.daily.name")
                .setLore(
                        player.getMessage("hub.gui.reward.daily.lore.info"),
                        player.getMessage(loreKey)
                )
                .build(), event -> {
            if(rewardPlayer.getDailyReward().isCollectable()) {
                this.rewardPlayer.getDailyReward().setLastCollected(System.currentTimeMillis());
                this.rewardPlayer.increaseRewardPoint();
                player.toBukkitPlayer().playSound(player.toBukkitPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 2, 1);
                savePlayer();
                this.setDailyRewardItem(player, inventory);

                //reopen to update title
                new RewardGuiIndex(this.rewardPlayer).open(player);
            }else {
                player.toBukkitPlayer().playSound(player.toBukkitPlayer().getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 2, 1);
            }
        });
    }
}
