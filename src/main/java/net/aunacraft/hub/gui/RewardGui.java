package net.aunacraft.hub.gui;

import net.aunacraft.api.gui.Gui;
import net.aunacraft.hub.HubPlugin;
import net.aunacraft.hub.reward.player.RewardPlayer;

public class RewardGui extends Gui {

    protected RewardPlayer rewardPlayer;

    public RewardGui(RewardPlayer rewardPlayer, String guiName, int size) {
        super(guiName, size);
        this.rewardPlayer = rewardPlayer;
    }

    protected void savePlayer() {
        HubPlugin.getInstance().getRewardPlayerRepository().save(this.rewardPlayer);
    }
}
