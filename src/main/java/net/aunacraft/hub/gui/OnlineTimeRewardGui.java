package net.aunacraft.hub.gui;

import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Getter;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.gui.page.PageGui;
import net.aunacraft.api.gui.page.Pageable;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.api.util.SkullBuilder;
import net.aunacraft.hub.HubPlugin;
import net.aunacraft.hub.reward.loginstreak.LoginStreak;
import net.aunacraft.hub.reward.loginstreak.LoginStreakReward;
import net.aunacraft.hub.reward.onlinetime.OnlineTimeReward;
import net.aunacraft.hub.reward.player.RewardPlayer;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class OnlineTimeRewardGui extends PageGui {

    private static final ItemStack ARROW_LEFT_ITEM_STACK = SkullBuilder.getSkull("http://textures.minecraft.net/texture/37aee9a75bf0df7897183015cca0b2a7d755c63388ff01752d5f4419fc645");

    private final RewardPlayer rewardPlayer;

    public OnlineTimeRewardGui(RewardPlayer player, List<OnlineTimeReward> rewards) {
        super(player.toAunaPlayer().getMessage("hub.gui.onlinetime.title", player.getLoginStreak().getStreak()), 9 * 2, Lists.newArrayList(convertRewardList(player, rewards)));
        this.rewardPlayer = player;
    }

    @Override
    public void preInit(AunaPlayer aunaPlayer, GuiInventory inventory, int invSize) {
        inventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName("§c ").build());
        for(int i = 9; i < 18; i++) {
            inventory.setItem(i, null);
        }
    }

    @Override
    public void postInit(AunaPlayer aunaPlayer, GuiInventory inventory, int invSize) {
        inventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName("§c ").build());
        inventory.setItem(18, new ItemBuilder(ARROW_LEFT_ITEM_STACK).setDisplayName(aunaPlayer, "api.gui.back").build(), event -> {
            new RewardGuiIndex(this.rewardPlayer).open(aunaPlayer);
        });
    }

    @Override
    protected int getAmountPerPage() {
        return 9;
    }

    private static List<Pageable> convertRewardList(RewardPlayer player, List<OnlineTimeReward> rewards) {
        List<Pageable> rewardPageables = Lists.newArrayList();
        rewards.forEach(reward -> rewardPageables.add(new OnlineTimeRewardPageable(reward, player)));
        return rewardPageables;
    }

    @Getter
    @AllArgsConstructor
    private static class OnlineTimeRewardPageable implements Pageable {

        private final OnlineTimeReward reward;
        private final RewardPlayer player;

        @Override
        public void onClick(PageGui pageGui, AunaPlayer aunaPlayer) {
            if (player.canCollectOnlineTimeReward(reward)) {
                player.setOnlineTimeRewardCollected(reward);
                reward.givePlayer(player);
                HubPlugin.getInstance().getRewardPlayerRepository().save(player);
                aunaPlayer.toBukkitPlayer().playSound(aunaPlayer.toBukkitPlayer().getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 2, 1);
                pageGui.inventory.clear();
                pageGui.initInventory(aunaPlayer, pageGui.inventory);
            }else {
                aunaPlayer.toBukkitPlayer().playSound(aunaPlayer.toBukkitPlayer().getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 2, 1);
            }
        }

        @Override
        public ItemStack getIconStack() {
            Material material = player.hasCollectedOnlineTimeReward(reward) ? Material.MINECART : player.canCollectOnlineTimeReward(reward) ? Material.CHEST_MINECART : Material.TNT_MINECART;

            ItemBuilder builder = new ItemBuilder(material).setDisplayName(player.toAunaPlayer(),"hub.gui.onlinetime.item.reward.name", TimeUnit.MILLISECONDS.toHours(this.reward.getTime()));
            if(player.canCollectOnlineTimeReward(reward)) {
                builder.setLore(player.toAunaPlayer().getMessage("hub.gui.onlinetime.item.reward.lore.collectable"));
            }else if(player.hasCollectedOnlineTimeReward(reward)) {
                builder.setLore(player.toAunaPlayer().getMessage("hub.gui.onlinetime.item.reward.lore.collected"));
            }else {
                builder.setLore(
                        player.toAunaPlayer().getMessage("hub.gui.onlinetime.item.reward.lore.not_available.1"),
                        player.toAunaPlayer().getMessage("hub.gui.onlinetime.item.reward.lore.not_available.2", TimeUnit.MILLISECONDS.toHours(this.reward.getTime()))
                );
            }

            return builder.build();
        }
    }
}
