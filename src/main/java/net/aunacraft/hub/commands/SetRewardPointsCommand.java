package net.aunacraft.hub.commands;

import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.Command;
import net.aunacraft.api.commands.autocompleetion.impl.PositiveIntegerCompletionHandler;
import net.aunacraft.api.commands.autocompleetion.impl.RegisteredPlayerCompletionHandler;
import net.aunacraft.api.commands.builder.CommandBuilder;
import net.aunacraft.api.commands.builder.ParameterBuilder;
import net.aunacraft.api.util.UUIDFetcher;
import net.aunacraft.hub.HubPlugin;

import java.util.UUID;

public class SetRewardPointsCommand implements AunaCommand {

    @Override
    public Command createCommand() {
        return CommandBuilder.beginCommand("setrewardpoints")
                .permission("hub.setrewardpoints")
                .parameter(
                        ParameterBuilder.beginParameter("player")
                                .required()
                                .autoCompletionHandler(new RegisteredPlayerCompletionHandler())
                                .build()
                )
                .parameter(
                        ParameterBuilder.beginParameter("points")
                                .autoCompletionHandler(new PositiveIntegerCompletionHandler())
                                .required()
                                .build()
                )
                .handler((executor, commandContext, strings) -> {
                    int points = commandContext.getParameterValue("points", Integer.class);
                    UUID targetUUID = commandContext.getParameterValue("player", UUID.class);
                    HubPlugin.getInstance().getRewardPlayerRepository().setRewardPoints(targetUUID, points);
                    executor.sendRawMessage("§7Set reward points of " + UUIDFetcher.getName(targetUUID) + " " + points);
                })
                .build();
    }
}
