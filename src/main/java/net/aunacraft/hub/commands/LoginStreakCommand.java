package net.aunacraft.hub.commands;

import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.Command;
import net.aunacraft.api.commands.autocompleetion.impl.PositiveIntegerCompletionHandler;
import net.aunacraft.api.commands.autocompleetion.impl.RegisteredPlayerCompletionHandler;
import net.aunacraft.api.commands.builder.CommandBuilder;
import net.aunacraft.api.commands.builder.ParameterBuilder;
import net.aunacraft.api.util.UUIDFetcher;
import net.aunacraft.hub.HubPlugin;
import net.aunacraft.hub.commands.completions.LoginStreakRewardCompletionHandler;
import net.aunacraft.hub.reward.loginstreak.LoginStreakReward;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class LoginStreakCommand implements AunaCommand {

    @Override
    public Command createCommand() {
        return CommandBuilder.beginCommand("loginstreak")
                .subCommand(
                        CommandBuilder.beginCommand("addreward")
                                .permission("hub.loginstreak.editreward")
                                .parameter(
                                        ParameterBuilder.beginParameter("streak")
                                                .autoCompletionHandler(new PositiveIntegerCompletionHandler(1, 2, 3))
                                                .required()
                                                .build()
                                )
                                .parameter(
                                        ParameterBuilder.beginParameter("reward-points")
                                                .autoCompletionHandler(new PositiveIntegerCompletionHandler(1, 2, 3))
                                                .required()
                                                .build()
                                )
                                .handler((executor, commandContext, strings) -> {
                                    int streak = commandContext.getParameterValue("streak", Integer.class);
                                    int rewardPoints = commandContext.getParameterValue("reward-points", Integer.class);
                                    LoginStreakReward reward = new LoginStreakReward(streak, rewardPoints);
                                    HubPlugin.getInstance().getLoginStreakHandler().addReward(reward);
                                    executor.sendRawMessage("§7Added reward (streak: " + reward.getStreak() + " / amount: " + reward.getAmount() + ")");
                                })
                                .build()
                )
                .subCommand(
                        CommandBuilder.beginCommand("removereward")
                                .permission("hub.loginstreak.editreward")
                                .parameter(
                                        ParameterBuilder.beginParameter("reward")
                                                .autoCompletionHandler(new LoginStreakRewardCompletionHandler())
                                                .required()
                                                .build()
                                )
                                .handler((executor, commandContext, strings) -> {
                                    LoginStreakReward reward = commandContext.getParameterValue("reward", LoginStreakReward.class);
                                    HubPlugin.getInstance().getLoginStreakHandler().removeReward(reward);
                                    executor.sendRawMessage("§7Removed reward (streak: " + reward.getStreak() + " / amount: " + reward.getAmount() + ")");
                                })
                                .build()
                )
                .subCommand(
                        CommandBuilder.beginCommand("setstreak")
                                .permission("hub.loginstreak.setstreak")
                                .parameter(
                                        ParameterBuilder.beginParameter("player")
                                                .required()
                                                .autoCompletionHandler(new RegisteredPlayerCompletionHandler())
                                                .build()
                                )
                                .parameter(
                                        ParameterBuilder.beginParameter("streak")
                                                .autoCompletionHandler(new PositiveIntegerCompletionHandler(1, 2, 3))
                                                .required()
                                                .build()
                                )
                                .handler((executor, commandContext, strings) -> {
                                    UUID targetUUID = commandContext.getParameterValue("player", UUID.class);
                                    int newStreak = commandContext.getParameterValue("streak", Integer.class);
                                    HubPlugin.getInstance().getRewardPlayerRepository().loadByOwner(targetUUID, player -> {
                                        player.getLoginStreak().setStreak(newStreak);
                                        executor.sendRawMessage("§7Set streak of " + UUIDFetcher.getName(targetUUID) + " to: " + player.getLoginStreak().getStreak());
                                        HubPlugin.getInstance().getRewardPlayerRepository().save(player);
                                    });
                                })
                                .build()
                )
                .subCommand(
                        CommandBuilder.beginCommand("test")
                                .permission("system.admin")
                                .handler((executor, commandContext, strings) -> {
                                    HubPlugin.getInstance().getRewardPlayerRepository().loadByOwner(executor.getUuid(), player -> {
                                        player.getLoginStreak().setLastJoin(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(1));
                                        HubPlugin.getInstance().getRewardPlayerRepository().save(player);
                                        executor.sendRawMessage("§7Set your last join to yesterday.");
                                    });
                                })
                                .build()
                )
                .handler((executor, commandContext, strings) -> {
                    HubPlugin.getInstance().getRewardPlayerRepository().loadLoginStreakByOwner(executor.getUuid(), streak -> {
                        executor.sendMessage("hub.command.loginstreak.show", streak);
                    });
                })
                .build();
    }
}
