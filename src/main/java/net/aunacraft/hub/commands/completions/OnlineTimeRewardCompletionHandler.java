package net.aunacraft.hub.commands.completions;

import com.google.common.collect.Lists;
import net.aunacraft.api.commands.autocompleetion.AutoCompletionHandler;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.hub.HubPlugin;
import net.aunacraft.hub.reward.loginstreak.LoginStreakReward;
import net.aunacraft.hub.reward.onlinetime.OnlineTimeReward;
import net.aunacraft.hub.reward.onlinetime.OnlineTimeRewardHandler;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class OnlineTimeRewardCompletionHandler implements AutoCompletionHandler<OnlineTimeReward> {

    @Override
    public List<String> getCompletions() {
        List<String> matches = Lists.newArrayList();
        HubPlugin.getInstance().getOnlineTimeRewardHandler().getRewards()
                .forEach(reward -> matches.add(String.valueOf(TimeUnit.MILLISECONDS.toHours(reward.getTime()))));
        return matches;
    }

    @Override
    public boolean isValid(String s) {
        try {
            long time = TimeUnit.HOURS.toMillis(Integer.parseInt(s));
            return HubPlugin.getInstance().getOnlineTimeRewardHandler().getRewardByTime(time) != null;
        }catch (Exception e) {
            return false;
        }
    }

    @Override
    public OnlineTimeReward convertString(String s) {
        long time = TimeUnit.HOURS.toMillis(Integer.parseInt(s));
        return HubPlugin.getInstance().getOnlineTimeRewardHandler().getRewardByTime(time);
    }

    @Override
    public String getErrorMessage(AunaPlayer player) {
        return "No reward found with this time";
    }
}
