package net.aunacraft.hub.commands.completions;

import com.google.common.collect.Lists;
import net.aunacraft.api.commands.autocompleetion.AutoCompletionHandler;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.hub.HubPlugin;
import net.aunacraft.hub.reward.loginstreak.LoginStreakReward;

import java.util.List;

public class LoginStreakRewardCompletionHandler implements AutoCompletionHandler<LoginStreakReward> {

    @Override
    public List<String> getCompletions() {
        List<String> matches = Lists.newArrayList();
        HubPlugin.getInstance().getLoginStreakHandler().getRewards().forEach(reward -> matches.add(String.valueOf(reward.getStreak())));
        return matches;
    }

    @Override
    public boolean isValid(String s) {
        try {
            int streak = Integer.parseInt(s);
            return HubPlugin.getInstance().getLoginStreakHandler().getRewardByStreak(streak) != null;
        }catch (Exception e) {
            return false;
        }
    }

    @Override
    public LoginStreakReward convertString(String s) {
        int streak = Integer.parseInt(s);
        return HubPlugin.getInstance().getLoginStreakHandler().getRewardByStreak(streak);
    }

    @Override
    public String getErrorMessage(AunaPlayer player) {
        return "No reward found with this streak";
    }
}
