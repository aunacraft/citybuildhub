package net.aunacraft.hub.commands;

import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.Command;
import net.aunacraft.api.commands.autocompleetion.impl.PositiveIntegerCompletionHandler;
import net.aunacraft.api.commands.builder.CommandBuilder;
import net.aunacraft.api.commands.builder.ParameterBuilder;
import net.aunacraft.hub.HubPlugin;
import net.aunacraft.hub.commands.completions.OnlineTimeRewardCompletionHandler;
import net.aunacraft.hub.reward.onlinetime.OnlineTimeReward;

import java.util.concurrent.TimeUnit;

public class OnlineTimeRewardCommand implements AunaCommand {

    @Override
    public Command createCommand() {
        return CommandBuilder.beginCommand("onlinetimereward")
                .permission("hub.onlinetimereward.edit")
                .subCommand(
                        CommandBuilder.beginCommand("add")
                                .parameter(
                                        ParameterBuilder.beginParameter("time")
                                                .autoCompletionHandler(new PositiveIntegerCompletionHandler(1, 2, 3))
                                                .required()
                                                .build()
                                )
                                .parameter(
                                        ParameterBuilder.beginParameter("reward-points")
                                                .autoCompletionHandler(new PositiveIntegerCompletionHandler(1, 2, 3))
                                                .required()
                                                .build()
                                )
                                .handler((executor, commandContext, strings) -> {
                                    int time = commandContext.getParameterValue("time", Integer.class);
                                    int rewardPoints = commandContext.getParameterValue("reward-points", Integer.class);
                                    OnlineTimeReward reward = new OnlineTimeReward(TimeUnit.HOURS.toMillis(time), rewardPoints);
                                    HubPlugin.getInstance().getOnlineTimeRewardHandler().addReward(reward);
                                    executor.sendRawMessage("§7Added reward (time: " + time + " Hours / amount: " + reward.getAmount() + ")");
                                })
                                .build()
                )
                .subCommand(
                        CommandBuilder.beginCommand("remove")
                                .parameter(
                                        ParameterBuilder.beginParameter("reward")
                                                .autoCompletionHandler(new OnlineTimeRewardCompletionHandler())
                                                .required()
                                                .build()
                                )
                                .handler((executor, commandContext, strings) -> {
                                    OnlineTimeReward reward = commandContext.getParameterValue("reward", OnlineTimeReward.class);
                                    HubPlugin.getInstance().getOnlineTimeRewardHandler().removeReward(reward);
                                    executor.sendRawMessage("§7Removed reward (time: " + TimeUnit.MILLISECONDS.toHours(reward.getTime()) + " / amount: " + reward.getAmount() + ")");
                                })
                                .build()
                )
                .build();
    }
}
