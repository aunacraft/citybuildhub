package net.aunacraft.hub;

import lombok.Getter;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.bukkit.location.SpigotLocationProvider;
import net.aunacraft.api.database.DatabaseHandler;
import net.aunacraft.api.database.config.impl.SpigotDatabaseConfig;
import net.aunacraft.hub.commands.LoginStreakCommand;
import net.aunacraft.hub.commands.OnlineTimeRewardCommand;
import net.aunacraft.hub.commands.SetRewardPointsCommand;
import net.aunacraft.hub.npc.HubNPCHandler;
import net.aunacraft.hub.repository.LoginStreakRewardsRepository;
import net.aunacraft.hub.repository.RewardPlayerRepository;
import net.aunacraft.hub.repository.RewardPlayerRepositoryImpl;
import net.aunacraft.hub.reward.loginstreak.LoginStreak;
import net.aunacraft.hub.reward.loginstreak.LoginStreakHandler;
import net.aunacraft.hub.reward.onlinetime.OnlineTimeRewardHandler;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.stream.Stream;

@Getter
public class HubPlugin extends JavaPlugin {

    @Getter
    private static HubPlugin instance;

    private DatabaseHandler databaseHandler;
    private RewardPlayerRepository rewardPlayerRepository;
    private LoginStreakHandler loginStreakHandler;
    private OnlineTimeRewardHandler onlineTimeRewardHandler;

    private SpigotLocationProvider locationProvider;
    private HubNPCHandler npcHandler;


    @Override
    public void onLoad() {
        instance = this;
    }

    @Override
    public void onEnable() {
        AunaAPI.getApi().registerLocationProvider(locationProvider = new SpigotLocationProvider(this, true));
        this.databaseHandler = new DatabaseHandler(new SpigotDatabaseConfig(this));
        this.rewardPlayerRepository = new RewardPlayerRepositoryImpl(databaseHandler);

        this.npcHandler = new HubNPCHandler(this.locationProvider);
        this.loginStreakHandler = new LoginStreakHandler(this);

        this.onlineTimeRewardHandler = new OnlineTimeRewardHandler(this);

        Stream.of(
                new LoginStreakCommand(),
                new SetRewardPointsCommand(),
                new OnlineTimeRewardCommand()
        ).forEach(aunaCommand -> AunaAPI.getApi().registerCommand(aunaCommand));

    }
}
