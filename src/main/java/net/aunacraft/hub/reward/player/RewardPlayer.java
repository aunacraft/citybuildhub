package net.aunacraft.hub.reward.player;

import lombok.Getter;
import lombok.Setter;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.hub.reward.dailyreward.DailyReward;
import net.aunacraft.hub.reward.loginstreak.LoginStreak;
import net.aunacraft.hub.reward.loginstreak.LoginStreakReward;
import net.aunacraft.hub.reward.onlinetime.OnlineTimeReward;

import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class RewardPlayer {

    private final UUID uuid;

    private long rewardPoints;

    private LoginStreak loginStreak;

    private DailyReward dailyReward;

    private List<Long> collectedOnlineTimeRewards;

    public RewardPlayer(UUID uuid, long rewardPoints, LoginStreak loginStreak, DailyReward dailyReward, List<Long> collectedOnlineTimeRewards) {
        this.uuid = uuid;
        this.rewardPoints = rewardPoints;
        this.loginStreak = loginStreak;
        this.dailyReward = dailyReward;
        this.collectedOnlineTimeRewards = collectedOnlineTimeRewards;
    }

    public void increaseRewardPoint() {
        this.rewardPoints++;
    }

    public void addRewardPoints(int amount) {
        this.rewardPoints += amount;
    }

    public void removeRewardPoints(int amount) {
        this.rewardPoints -= amount;
        if(rewardPoints < 0)
            rewardPoints = 0;
    }

    public void setOnlineTimeRewardCollected(OnlineTimeReward reward) {
        this.collectedOnlineTimeRewards.add(reward.getTime());
    }

    public boolean hasCollectedOnlineTimeReward(OnlineTimeReward reward) {
        return this.collectedOnlineTimeRewards.contains(reward.getTime());
    }

    public boolean canCollectOnlineTimeReward(OnlineTimeReward reward) {
        long onlineTime = toAunaPlayer().getOnlineTime();
        return !hasCollectedOnlineTimeReward(reward) && onlineTime >= reward.getTime();
    }

    public AunaPlayer toAunaPlayer() {
        return AunaAPI.getApi().getPlayer(this.uuid);
    }
}
