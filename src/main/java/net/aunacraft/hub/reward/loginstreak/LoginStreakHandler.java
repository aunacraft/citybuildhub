package net.aunacraft.hub.reward.loginstreak;

import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.Getter;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.event.impl.PlayerRegisterEvent;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.api.message.PluginMessage;
import net.aunacraft.hub.HubPlugin;
import net.aunacraft.hub.repository.LoginStreakRewardsRepository;
import net.aunacraft.hub.repository.LoginStreakRewardsRepositoryImpl;
import net.aunacraft.hub.repository.RewardPlayerRepository;
import org.bukkit.Bukkit;

import java.io.File;
import java.lang.reflect.Type;
import java.util.List;

@Getter
public class LoginStreakHandler {

    private static final String UPDATE_TYPE = "LOGIN_STREAK_REWARD_UPDATE";

    private List<LoginStreakReward> rewards;
    private LoginStreakRewardsRepository loginStreakRewardsRepository;

    public LoginStreakHandler(HubPlugin plugin) {
        this.loginStreakRewardsRepository = new LoginStreakRewardsRepositoryImpl(plugin.getDatabaseHandler());
        loadRewards();
        listenPlayerRegister(plugin.getRewardPlayerRepository());
        listenUpdateChannel();
    }

    private void loadRewards() {
        HubPlugin.getInstance().getLogger().info("Loading all login reward");
        this.rewards = this.loginStreakRewardsRepository.loadAll();
    }

    public void addReward(LoginStreakReward reward) {
        this.loginStreakRewardsRepository.save(reward, this::sendUpdate);
    }

    public void removeReward(LoginStreakReward reward) {
        this.loginStreakRewardsRepository.delete(reward, this::sendUpdate);
    }

    private void sendUpdate() {
        AunaCloudAPI.getMessageAPI().sendPluginMessageToAllServices(new PluginMessage(UPDATE_TYPE));
    }

    public void listenPlayerRegister(RewardPlayerRepository repository) {
        AunaAPI.getApi().registerEventListener(PlayerRegisterEvent.class, event -> {
            repository.loadByOwner(event.getPlayer().getUuid(),rewardPlayer -> {
                LoginStreak loginStreak = rewardPlayer.getLoginStreak();
                if(loginStreak.shouldIncrease()) {
                    loginStreak.increaseStreak();
                    loginStreak.setLastJoin(System.currentTimeMillis());
                    event.getPlayer().sendMessage("hub.loginstreak.increased", loginStreak.getStreak());
                }else if(loginStreak.shouldReset()) {
                    event.getPlayer().sendMessage("hub.loginstreak.reseted", loginStreak.getStreak());
                    loginStreak.setLastJoin(System.currentTimeMillis());
                    loginStreak.reset();
                }else
                    return;
                repository.save(rewardPlayer);
            });
        });
    }

    public void listenUpdateChannel() {
        AunaCloudAPI.getMessageAPI().registerListener(pluginMessage -> {
            if(pluginMessage.getType().equals(UPDATE_TYPE)) {
                this.loadRewards();
            }
        });
    }

    public LoginStreakReward getRewardByStreak(int streak) {
        return this.rewards.stream().filter(reward -> reward.getStreak() == streak).findAny().orElse(null);
    }
}
