package net.aunacraft.hub.reward.loginstreak;

import com.google.common.collect.Lists;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;
import java.util.zip.GZIPOutputStream;

@Getter
@Setter
public class LoginStreak {

    private static final Gson GSON = new Gson();
    private static final Type LIST_TYPE = new TypeToken<List<Integer>>(){}.getType();

    private long lastJoin;

    private long streak;

    private List<Integer> collectedRewards;

    public LoginStreak(long lastJoin, long streak, List<Integer> collectedRewards) {
        this.lastJoin = lastJoin;
        this.streak = streak;
        this.collectedRewards = collectedRewards;
    }

    public boolean shouldIncrease() {
        Timestamp lastCollected = new Timestamp(this.lastJoin);
        LocalDateTime lastCollectedDate = lastCollected.toLocalDateTime();
        LocalDate now = LocalDate.now();

        int lastCollectedDay = (lastCollectedDate.getYear() - 1) * 365 + lastCollectedDate.getDayOfYear();
        int currentDay = (now.getYear() - 1) * 365 + now.getDayOfYear();

        return currentDay == lastCollectedDay + 1;

    }

    public boolean shouldReset() {
        Timestamp lastCollected = new Timestamp(this.lastJoin);
        LocalDateTime lastCollectedDate = lastCollected.toLocalDateTime();
        LocalDate now = LocalDate.now();

        int lastCollectedDay = (lastCollectedDate.getYear() - 1) * 365 + lastCollectedDate.getDayOfYear();
        int currentDay = (now.getYear() - 1) * 365 + now.getDayOfYear();
        return currentDay != lastCollectedDay + 1 && currentDay != lastCollectedDay && this.streak > 0;
    }

    public void increaseStreak() {
        this.streak++;
    }

    public void setRewardCollected(LoginStreakReward reward) {
        this.collectedRewards.add(reward.getStreak());
    }

    public boolean hasCollectedReward(LoginStreakReward reward) {
        return this.collectedRewards.contains(reward.getStreak());
    }

    public boolean canCollectReward(LoginStreakReward reward) {
        return !hasCollectedReward(reward) && this.streak >= reward.getStreak();
    }

    public void reset() {
        this.streak = 0;
        this.collectedRewards.clear();
    }

    public static String collectedRewardsToString(List<Integer> collectedRewards) {
        return GSON.toJson(collectedRewards, LIST_TYPE);
    }

    public static List<Integer> collectedRewardsFromString(String json) {
        return GSON.fromJson(json, LIST_TYPE);
    }
}
