package net.aunacraft.hub.reward.loginstreak;

import com.google.gson.annotations.SerializedName;
import lombok.Data;
import net.aunacraft.api.gui.page.PageGui;
import net.aunacraft.api.gui.page.Pageable;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.hub.reward.player.RewardPlayer;
import org.bukkit.inventory.ItemStack;

@Data
public class LoginStreakReward {

    private final int streak;
    private final int amount;

    public void givePlayer(RewardPlayer player) {
        player.addRewardPoints(this.amount);
    }
}
