package net.aunacraft.hub.reward.onlinetime;

import lombok.Data;
import lombok.Getter;
import net.aunacraft.hub.reward.player.RewardPlayer;

@Getter
@Data
public class OnlineTimeReward {

    private final long time;
    private final int amount;

    public void givePlayer(RewardPlayer player) {
        player.addRewardPoints(this.amount);
    }
}
