package net.aunacraft.hub.reward.onlinetime;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import lombok.Getter;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.event.impl.PlayerRegisterEvent;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.api.message.PluginMessage;
import net.aunacraft.hub.HubPlugin;
import net.aunacraft.hub.repository.*;
import net.aunacraft.hub.reward.loginstreak.LoginStreak;
import net.aunacraft.hub.reward.loginstreak.LoginStreakReward;
import org.bukkit.Bukkit;

import java.lang.reflect.Type;
import java.util.List;

@Getter
public class OnlineTimeRewardHandler {

    private static final Gson GSON = new Gson();
    private final static Type LIST_TYPE = new TypeToken<List<Long>>(){}.getType();
    private static final String UPDATE_TYPE = "ONLINE_TIME_REWARD_UPDATE";

    private List<OnlineTimeReward> rewards;
    private final OnlineTimeRewardRepository repository;

    public OnlineTimeRewardHandler(HubPlugin plugin) {
        this.repository = new OnlineTimeRewardRepositoryImpl(plugin.getDatabaseHandler());
        loadRewards();
        listenUpdateChannel();
    }

    private void loadRewards() {
        HubPlugin.getInstance().getLogger().info("Loading all onlinetime rewards");
        this.rewards = this.repository.loadAll();
    }

    public void addReward(OnlineTimeReward reward) {
        this.repository.save(reward, this::sendUpdate);
    }

    public void removeReward(OnlineTimeReward reward) {
        this.repository.delete(reward, this::sendUpdate);
    }

    private void sendUpdate() {
        AunaCloudAPI.getMessageAPI().sendPluginMessageToAllServices(new PluginMessage(UPDATE_TYPE));
    }

    public void listenUpdateChannel() {
        AunaCloudAPI.getMessageAPI().registerListener(pluginMessage -> {
            if(pluginMessage.getType().equals(UPDATE_TYPE)) {
                this.loadRewards();
            }
        });
    }

    public OnlineTimeReward getRewardByTime(long time) {
        return this.rewards.stream().filter(reward -> reward.getTime() == time).findAny().orElse(null);
    }

    public static String collectedRewardsToString(List<Long> collectedRewards) {
        return GSON.toJson(collectedRewards, LIST_TYPE);
    }

    public static List<Long> collectedRewardsFromString(String json) {
        return GSON.fromJson(json, LIST_TYPE);
    }
}
