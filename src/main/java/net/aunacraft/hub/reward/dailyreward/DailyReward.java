package net.aunacraft.hub.reward.dailyreward;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
public class DailyReward {
    private long lastCollected;

    public DailyReward(long lastCollected) {
        this.lastCollected = lastCollected;
    }

    public boolean isCollectable() {
        Timestamp lastCollected = new Timestamp(this.lastCollected);
        LocalDateTime lastCollectedDate = lastCollected.toLocalDateTime();
        LocalDate now = LocalDate.now();
        return lastCollectedDate.getDayOfYear() != now.getDayOfYear() || lastCollectedDate.getYear() != now.getYear();
    }
}
