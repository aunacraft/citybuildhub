package net.aunacraft.hub.repository;

import com.google.common.collect.Lists;
import net.aunacraft.api.database.DatabaseHandler;
import net.aunacraft.hub.reward.loginstreak.LoginStreakReward;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class LoginStreakRewardsRepositoryImpl implements LoginStreakRewardsRepository {

    private final DatabaseHandler databaseHandler;

    public LoginStreakRewardsRepositoryImpl(DatabaseHandler databaseHandler) {
        this.databaseHandler = databaseHandler;
        crateTables();
    }

    public void crateTables() {
        this.databaseHandler.createBuilder("CREATE TABLE IF NOT EXISTS hub_login_streak_rewards (streak INT PRIMARY KEY , amount INT)")
                .updateSync();
    }


    @Override
    public List<LoginStreakReward> loadAll() {
        List<LoginStreakReward> rewards = Lists.newArrayList();
        ResultSet rs = this.databaseHandler.createBuilder("SELECT * FROM hub_login_streak_rewards").querySync();
        try {
            while (rs.next()) {
                rewards.add(new LoginStreakReward(rs.getInt("streak"), rs.getInt("amount")));
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return rewards;
    }

    @Override
    public void save(LoginStreakReward loginStreakReward, Runnable callback) {
        this.databaseHandler.createBuilder("SELECT streak FROM hub_login_streak_rewards WHERE streak=?")
                .addObjects(loginStreakReward.getStreak())
                .queryAsync(existsRs -> {
                    try {
                        boolean exists = existsRs.next();
                        if(exists) {
                            this.databaseHandler.createBuilder("UPDATE hub_login_streak_rewards SET amount=? WHERE streak=?")
                                    .addObjects(loginStreakReward.getAmount(), loginStreakReward.getStreak())
                                    .updateAsync(callback);
                        }else {
                            this.databaseHandler.createBuilder("INSERT INTO hub_login_streak_rewards (streak, amount) VALUES (?, ?)")
                                    .addObjects(loginStreakReward.getStreak(), loginStreakReward.getAmount())
                                    .updateAsync(callback);
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });
    }

    @Override
    public void delete(LoginStreakReward reward, Runnable callback) {
        this.databaseHandler.createBuilder("DELETE FROM hub_login_streak_rewards WHERE streak=?")
                .addObjects(reward.getStreak())
                .updateAsync(callback);
    }
}
