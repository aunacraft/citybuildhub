package net.aunacraft.hub.repository;

import com.google.common.collect.Lists;
import net.aunacraft.api.database.DatabaseHandler;
import net.aunacraft.hub.repository.RewardPlayerRepository;
import net.aunacraft.hub.reward.dailyreward.DailyReward;
import net.aunacraft.hub.reward.loginstreak.LoginStreak;
import net.aunacraft.hub.reward.onlinetime.OnlineTimeRewardHandler;
import net.aunacraft.hub.reward.player.RewardPlayer;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

public class RewardPlayerRepositoryImpl implements RewardPlayerRepository {

    private final DatabaseHandler databaseHandler;

    public RewardPlayerRepositoryImpl(DatabaseHandler databaseHandler) {
        this.databaseHandler = databaseHandler;
        createTables();
    }

    private void createTables() {
        databaseHandler.createBuilder("CREATE TABLE IF NOT EXISTS hub_reward_player (uuid VARCHAR(100), reward_points BIGINT, daily_reward_last_collected BIGINT, login_streak_streak BIGINT, login_streak_last_join BIGINT, login_streak_collected_rewards TEXT, collected_online_time_rewards TEXT)").updateSync();
    }

    @Override
    public void loadByOwner(UUID uuid, Consumer<RewardPlayer> playerConsumer) {
        this.databaseHandler.createBuilder("SELECT * FROM hub_reward_player WHERE uuid=?")
                .addObjects(uuid.toString())
                .queryAsync(rs -> {
                    try {
                        long rewardPoints;
                        long dailyRewardLastCollected;
                        long loginStreakStreak;
                        long loginStreakLastJoin;
                        List<Integer> loginStreakCollectedRewards;
                        List<Long> onlineTimeCollectedRewards;
                        if(rs.next()) {
                            rewardPoints = rs.getLong("reward_points");
                            dailyRewardLastCollected = rs.getLong("daily_reward_last_collected");
                            loginStreakStreak = rs.getLong("login_streak_streak");
                            loginStreakLastJoin = rs.getLong("login_streak_last_join");
                            loginStreakCollectedRewards = LoginStreak.collectedRewardsFromString(rs.getString("login_streak_collected_rewards"));
                            onlineTimeCollectedRewards = OnlineTimeRewardHandler.collectedRewardsFromString(rs.getString("collected_online_time_rewards"));
                        }else {
                            rewardPoints = 0;
                            dailyRewardLastCollected = System.currentTimeMillis() - TimeUnit.DAYS.toMillis(2);
                            loginStreakStreak = 0;
                            loginStreakLastJoin = System.currentTimeMillis();
                            loginStreakCollectedRewards = Lists.newArrayList();
                            onlineTimeCollectedRewards = Lists.newArrayList();

                            this.databaseHandler.createBuilder("INSERT INTO hub_reward_player (uuid, reward_points, daily_reward_last_collected, login_streak_streak, login_streak_last_join, login_streak_collected_rewards, collected_online_time_rewards) VALUES (?, ?, ?, ?, ?, ?, ?)")
                                    .addObjects(
                                            uuid.toString(),
                                            rewardPoints,
                                            dailyRewardLastCollected,
                                            loginStreakStreak,
                                            loginStreakLastJoin,
                                            LoginStreak.collectedRewardsToString(loginStreakCollectedRewards),
                                            OnlineTimeRewardHandler.collectedRewardsToString(onlineTimeCollectedRewards)
                                    ).updateSync();
                        }

                        LoginStreak loginStreak = new LoginStreak(loginStreakLastJoin, loginStreakStreak, loginStreakCollectedRewards);
                        DailyReward dailyReward = new DailyReward(dailyRewardLastCollected);

                        playerConsumer.accept(new RewardPlayer(uuid, rewardPoints, loginStreak, dailyReward, onlineTimeCollectedRewards));
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });
    }

    @Override
    public void loadLoginStreakByOwner(UUID owner, Consumer<Integer> consumer) {
        this.databaseHandler.createBuilder("SELECT login_streak_streak FROM hub_reward_player WHERE uuid=?")
                .addObjects(owner.toString())
                .queryAsync(rs -> {
                    try {
                        if(rs.next()) {
                            consumer.accept(rs.getInt("login_streak_streak"));
                            return;
                        }
                    }catch (SQLException e) {
                        e.printStackTrace();
                    }
                    consumer.accept(0);
                });
    }

    @Override
    public void save(RewardPlayer player) {
        this.databaseHandler.createBuilder("UPDATE hub_reward_player SET reward_points=?, daily_reward_last_collected=?, login_streak_streak=?, login_streak_last_join=?, login_streak_collected_rewards=?, collected_online_time_rewards=? WHERE uuid=?")
                .addObjects(
                        player.getRewardPoints(),
                        player.getDailyReward().getLastCollected(),
                        player.getLoginStreak().getStreak(),
                        player.getLoginStreak().getLastJoin(),
                        LoginStreak.collectedRewardsToString(player.getLoginStreak().getCollectedRewards()),
                        OnlineTimeRewardHandler.collectedRewardsToString(player.getCollectedOnlineTimeRewards()),
                        player.getUuid().toString()
                )
                .updateAsync();
    }

    @Override
    public void setRewardPoints(UUID owner, int points) {
        this.databaseHandler.createBuilder("UPDATE hub_reward_player SET reward_points=? WHERE uuid=?")
                .addObjects(owner.toString())
                .updateAsync();
    }
}
