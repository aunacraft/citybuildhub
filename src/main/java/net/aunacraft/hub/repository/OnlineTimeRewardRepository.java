package net.aunacraft.hub.repository;

import net.aunacraft.hub.reward.loginstreak.LoginStreakReward;
import net.aunacraft.hub.reward.onlinetime.OnlineTimeReward;

import java.util.List;

public interface OnlineTimeRewardRepository {

    List<OnlineTimeReward> loadAll();

    void save(OnlineTimeReward onlineTimeReward, Runnable callback);

    void delete(OnlineTimeReward onlineTimeReward, Runnable callback);

}
