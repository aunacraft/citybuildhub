package net.aunacraft.hub.repository;

import com.google.common.collect.Lists;
import net.aunacraft.api.database.DatabaseHandler;
import net.aunacraft.hub.reward.loginstreak.LoginStreakReward;
import net.aunacraft.hub.reward.onlinetime.OnlineTimeReward;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class OnlineTimeRewardRepositoryImpl implements OnlineTimeRewardRepository {

    private final DatabaseHandler databaseHandler;

    public OnlineTimeRewardRepositoryImpl(DatabaseHandler databaseHandler) {
        this.databaseHandler = databaseHandler;
        crateTables();
    }

    public void crateTables() {
        this.databaseHandler.createBuilder("CREATE TABLE IF NOT EXISTS hub_online_time_rewards (time BIGINT PRIMARY KEY , amount INT)")
                .updateSync();
    }


    @Override
    public List<OnlineTimeReward> loadAll() {
        List<OnlineTimeReward> rewards = Lists.newArrayList();
        ResultSet rs = this.databaseHandler.createBuilder("SELECT * FROM hub_online_time_rewards").querySync();
        try {
            while (rs.next()) {
                rewards.add(new OnlineTimeReward(rs.getLong("time"), rs.getInt("amount")));
            }
        }catch (SQLException e) {
            e.printStackTrace();
        }
        return rewards;
    }

    @Override
    public void save(OnlineTimeReward onlineTimeReward, Runnable callback) {
        this.databaseHandler.createBuilder("SELECT amount FROM hub_online_time_rewards WHERE time=?")
                .addObjects(onlineTimeReward.getTime())
                .queryAsync(existsRs -> {
                    try {
                        boolean exists = existsRs.next();
                        if(exists) {
                            this.databaseHandler.createBuilder("UPDATE hub_online_time_rewards SET amount=? WHERE time=?")
                                    .addObjects(onlineTimeReward.getAmount(), onlineTimeReward.getTime())
                                    .updateAsync(callback);
                        }else {
                            this.databaseHandler.createBuilder("INSERT INTO hub_online_time_rewards (time, amount) VALUES (?, ?)")
                                    .addObjects(onlineTimeReward.getTime(), onlineTimeReward.getAmount())
                                    .updateAsync(callback);
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                });
    }

    @Override
    public void delete(OnlineTimeReward reward, Runnable callback) {
        this.databaseHandler.createBuilder("DELETE FROM hub_online_time_rewards WHERE time=?")
                .addObjects(reward.getTime())
                .updateAsync(callback);
    }
}
