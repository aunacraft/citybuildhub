package net.aunacraft.hub.repository;

import net.aunacraft.hub.reward.loginstreak.LoginStreakReward;
import net.aunacraft.hub.reward.player.RewardPlayer;

import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;

public interface LoginStreakRewardsRepository {

    List<LoginStreakReward> loadAll();

    void save(LoginStreakReward loginStreakReward, Runnable callback);

    void delete(LoginStreakReward reward, Runnable callback);

}
