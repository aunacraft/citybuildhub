package net.aunacraft.hub.repository;

import net.aunacraft.hub.reward.player.RewardPlayer;

import java.util.UUID;
import java.util.function.Consumer;

public interface RewardPlayerRepository {

    void loadByOwner(UUID owner, Consumer<RewardPlayer> playerConsumer);

    void loadLoginStreakByOwner(UUID owner, Consumer<Integer> consumer);

    void save(RewardPlayer player);

    void setRewardPoints(UUID owner, int points);

}
