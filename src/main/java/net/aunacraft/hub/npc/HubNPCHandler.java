package net.aunacraft.hub.npc;

import com.google.common.collect.Lists;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.bukkit.location.SpigotLocationProvider;
import net.aunacraft.api.event.impl.LocationSetEvent;
import net.aunacraft.hub.npc.impl.RewardNPC;
import org.bukkit.Location;

import java.util.List;
import java.util.stream.Stream;

public class HubNPCHandler {

    private final List<HubNPC> hubNPCs = Lists.newArrayList();
    private final SpigotLocationProvider locationProvider;

    public HubNPCHandler(SpigotLocationProvider locationProvider) {
        this.locationProvider = locationProvider;
        Stream.of(
                new RewardNPC()
        ).forEach(this::registerNPC);


        AunaAPI.getApi().registerEventListener(LocationSetEvent.class, event -> {
            handleLocationLoad(event.getKey(), event.getLocation());
        });
    }

    private void registerNPC(HubNPC npc) {
        this.hubNPCs.add(npc);
        if (locationProvider.hasLocationSaved(npc.getLocationIdentifier())) {
            locationProvider.getLocationAsync(npc.getLocationIdentifier(), location -> {
                handleLocationLoad(npc.getLocationIdentifier(), location);
            });
        }
    }

    private void handleLocationLoad(String identifier, Location location) {
        this.hubNPCs.stream().filter(hubNPC -> hubNPC.getLocationIdentifier().equals(identifier)).findAny().ifPresent(hubNPC -> {
            hubNPC.load(location);
        });
    }
}
