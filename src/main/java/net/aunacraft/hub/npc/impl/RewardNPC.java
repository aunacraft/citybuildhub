package net.aunacraft.hub.npc.impl;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.bukkit.npc.builder.NPCBuilder;
import net.aunacraft.api.bukkit.npc.event.NPCInteractEvent;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.SkinFetcher;
import net.aunacraft.hub.HubPlugin;
import net.aunacraft.hub.gui.RewardGuiIndex;
import net.aunacraft.hub.npc.HubNPC;
import org.bukkit.Location;

import java.util.Random;

public class RewardNPC implements HubNPC {

    @Override
    public String getLocationIdentifier() {
        return "reward_npc";
    }

    @Override
    public void load(Location location) {
        new NPCBuilder(HubPlugin.getInstance())
                .setSkin(new SkinFetcher.SkinObject(
                                "ewogICJ0aW1lc3RhbXAiIDogMTU5ODgzMzI4MTYxNiwKICAicHJvZmlsZUlkIiA6ICIxNzhmMTJkYWMzNTQ0ZjRhYjExNzkyZDc1MDkzY2JmYyIsCiAgInByb2ZpbGVOYW1lIiA6ICJzaWxlbnRkZXRydWN0aW9uIiwKICAic2lnbmF0dXJlUmVxdWlyZWQiIDogdHJ1ZSwKICAidGV4dHVyZXMiIDogewogICAgIlNLSU4iIDogewogICAgICAidXJsIiA6ICJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2JmOTQ3ZTQ1ZWU1YWZhOThiY2UyMDc3ZjEzNTNjNWE4YWJhOWNhMjkzZDJhMWE1MzI0ZTkxM2Y2MzQ3MzhmY2QiCiAgICB9CiAgfQp9",
                                "dU4SCX0N6tJFYRpcd/W49MU2ykCQWDgdkQWtzCaBGtDMTakZpiqMYVOZVd7RyrONUWYF9m4+MS6dDnisGFwoaCfKxlt7Iu84PlJ62veE16+s/NbfZBT5WsINPTsCH9XWtg5119aRJv5+QHxt8rL17DnfFIuGTkxs9w07SO4Sf4iXpmdMZfIqwpZlm7qInLgMEo2+t5MiwAmMWBKI+avaKbXFe0j6VhnrMeSS2Ofk5NlpxqLRzcH7g9s6DoNgLoeAYAhJGR7QUZRBcKXK/DzBd8IieHwtADFSNtbUyI7/sfVuzO64cf7MlYABrYFDzdkT1QGTAAprSDLWyB1Tm+ZnGQ9YUSdz+DbPz4nWylTx2pkjP0DeezNzDzSIZYhr5L0ohnhljUmEbfryzKMCYEnwgplAx72qoevPhXlUpaa+Pmsmkjfaa18PEsehtsg9aJn5MJmotWxz72vj7BX7eCcBzL7Kmhf6WjZs1/xJhgi5lutp2S3nS1PN91pEDvD7dbKwnxPWkdNxw43k/6fcAwOcBgwDzDuy0nVx0lVbKXDy4ez51NNrfL3HaBzUcOHdlrq2xF8NyC4TBG7Zg6yZgc8DAu59fLT9SEALB2KufLaWSdrQ2dWs5YFluoDTOBQpLG8h7OX6KiBoNxJEzPGJXAVYrcKcBV9uXCQ1g/CIefjxX9U="
                        ))
                .setNameKey("hub.reward")
                .setLocation(location)
                .setPunchPlayers(true)
                .setLookAtPlayer(true)
                .setInteractListener(this)
                .build();
    }

    @Override
    public void interact(NPCInteractEvent npcInteractEvent) {
        AunaPlayer player = AunaAPI.getApi().getPlayer(npcInteractEvent.getPlayer().getUniqueId());
        if(player == null)
            return;

        HubPlugin.getInstance().getRewardPlayerRepository().loadByOwner(player.getUuid(), rewardPlayer -> {
            new RewardGuiIndex(rewardPlayer).open(player);
        });

    }
}
