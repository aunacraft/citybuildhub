package net.aunacraft.hub.npc;

import net.aunacraft.api.bukkit.npc.event.NPCInteractListener;
import org.bukkit.Location;

public interface HubNPC extends NPCInteractListener {

    String getLocationIdentifier();

    void load(Location location);

}
